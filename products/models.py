# -*- coding: utf-8 -*-

from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Product(models.Model):
    name = models.CharField(max_length=250, verbose_name=u'Наименование')
    description = models.TextField(null=True, verbose_name=u'Описание')
    properties = models.TextField(null=True, verbose_name=u'Характеристики')
    pics = models.TextField(null=True, verbose_name=u'Фото')
    price = models.FloatField(null=True, verbose_name=u'Цена')
    category = models.ForeignKey(Category, default=None, verbose_name=u'Категория')
    product_code = models.CharField(null=True, max_length=250, verbose_name=u'Артикул')
    url = models.TextField(verbose_name=u'Адрес', null=True, blank=True)
    brand = models.CharField(max_length=100, verbose_name=u'Производитель', null=True)
    barcode = models.CharField(max_length=250, null=True, blank=True)
    quantity = models.IntegerField(null=True, blank=True)
    extra_categories = models.ManyToManyField(Category, related_name='extra_categories')


class Category(MPTTModel):
    name = models.CharField(max_length=250, verbose_name=u'Наименование')
    url = models.TextField(verbose_name=u'Адрес')
    parent = TreeForeignKey('self', null=True, verbose_name=u'Родительская категория')
