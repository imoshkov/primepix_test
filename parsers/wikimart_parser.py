# -*- coding: utf-8 -*-

import json
import uuid
import time
import urllib
from django.conf import settings

from parsers.base_parser import BaseParser, ApiKeyDoesntSet


class WikimartParser(BaseParser):
    """
    Парсер из Wikimart'а

    Доки по API тут: http://affiliate.wikimart.ru/api/docs/
    """

    _API_KEY = None
    _API_URL = 'http://wikimart.apisystem.ru/'

    _GET_CATEGORIES_BY_CRITERIA = 'GetCategoriesByCriteria'
    _GET_CATEGORY_FILTERS = 'GetCategoryFilters'
    _GET_MODEL_BY_ID = 'GetModelById'
    _GET_MODEL_OFFERS = 'GetModelOffers'
    _GET_MODELS_BY_CRITERIA = 'GetModelsByCriteria'
    _GET_OFFERS_BY_IDS = 'GetOffersByIds'
    _SEARCH_MODELS_BY_CRITERIA = 'SearchModelsByCriteria'

    def __init__(self, api_key=None):
        super(WikimartParser, self).__init__()

        if api_key:
            self._API_KEY = api_key
        elif hasattr(settings, 'WIKIMART_API_KEY'):
            self._API_KEY = settings.WIKIMART_API_KEY
        else:
            raise ApiKeyDoesntSet()

    def _build_url(self, data):
        """Подготавливет URL запроса.

        Т.к. все запросы у нас GET, то просто добавляет GET параметры к URL'у

        :param data:
        :return:
        """
        return self._API_URL+'?'+urllib.urlencode(data)

    def _build_data(self, method, data):
        """Подготавливает данные к отправке запроса.

        Добавляет ключ API, метод и версию

        :param str method: метод API, см. доки по API
        :param dict data: словарь с запросом
        :return dict: подготовленный к запрос словарь с данными
        """

        data.update({
            'api_key': self._API_KEY,
            'timestamp': int(time.time()),
            'method': method,
            'version': '5.0',
        })

        if 'transaction_id' not in data:
            data['transaction_id'] = uuid.uuid4().__str__(),

        return data

    def _build_request(self, method, data):
        """Отправка запроса к API

        Вернёт False если код ответа не 200. Иначе вернёт распарсеный словарь с ответом API

        :param str method: метод API, см. доки по API
        :param dict data: словарь с запросом
        :return mix:
        """
        data = self._build_data(method, data)

        request_code, request_body = self._request('get', self._build_url(data), {'Content-Type': 'text/json'})

        print "\n"
        print request_body
        print "\n"

        if request_code == 200:
            data = json.loads(request_body)
            if 'success' in data:
                return data.get('response', []), data['transaction_id']

        return False, False

    def search_products(self, search_string, page=1, transaction_id=None):
        """Поиск продуктов

        :param str search_string: поисковая строка
        :param int page: номер страницы
        :return:
        """

        data = {
            'query': search_string,
            # 'args': {
            #     'query': search_string,
            #     'page': page
            # },
        }

        if page > 1:
            data['page'] = page

        if transaction_id:
            data['transaction_id'] = transaction_id

        response, transaction_id = self._build_request(self._SEARCH_MODELS_BY_CRITERIA, data)

        if not response:
            return False

        models = response.get('models', [])
        total_found = response.get('total_found', 0)
        found_by_type = response.get('found_by_type', [])
        total_pages = response.get('total_pages', 1)

        print page
        print transaction_id
        print len(models), total_found, total_pages
        print models
        print found_by_type

        if page < total_pages:
            page += 1
            self.search_products(search_string, page, transaction_id)
