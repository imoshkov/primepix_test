# -*- coding: utf-8 -*-

import json
import math
import urllib
from django.conf import settings

from parsers.base_parser import BaseParser, ApiKeyDoesntSet


class MailRuParser(BaseParser):
    """
    Парсер из Mail.ru

    Доки по API тут: http://torg.mail.ru/info/217/
    """

    _API_KEY = None
    _API_URL = 'http://mailru.apisystem.ru/'

    _API_VERSION = '1.0'
    _SEARCH_RESOURCE = '/search.json'

    def __init__(self, api_key=None):
        super(MailRuParser, self).__init__()

        if api_key:
            self._API_KEY = api_key
        elif hasattr(settings, 'MAILRU_API_KEY'):
            self._API_KEY = settings.MAILRU_API_KEY
        else:
            raise ApiKeyDoesntSet()

    def _build_url(self, resource, data):
        """Подготавливет URL запроса.

        Т.к. все запросы у нас GET, то просто добавляет GET параметры к URL'у

        :param data:
        :return:
        """
        return self._API_URL+self._API_VERSION+resource+'?'+urllib.urlencode(data)

    def _build_data(self, data):
        """Подготавливает данные к отправке запроса.

        Добавляет ключ API, метод и версию

        :param dict data: словарь с запросом
        :return dict: подготовленный к запрос словарь с данными
        """

        data.update({
            'api_key': self._API_KEY,
            'results_per_page': 30,
            'geo_id': 24,
        })

        return data

    def _build_request(self, resource, data):
        """Отправка запроса к API

        Вернёт False если код ответа не 200. Иначе вернёт распарсеный словарь с ответом API

        :param str resource: ресурс API, см. доки по API
        :param dict data: словарь с запросом
        :return mix:
        """
        data = self._build_data(data)

        request_code, request_body = self._request('get',
                                                   self._build_url(resource, data),
                                                   {'Content-Type': 'text/json'})

        print "\n"
        print request_body
        print "\n"

        if request_code == 200:
            data = json.loads(request_body)

            return data.get('searchResult', False)

        return False

    def search_products(self, search_string, page=1):
        """Поиск продуктов

        :param str search_string: поисковая строка
        :param int page: номер страницы
        :return:
        """

        data = {
            'query': search_string,
        }

        if page > 1:
            data['page'] = page

        response = self._build_request(self._SEARCH_RESOURCE, data)

        if not response:
            return False

        listing = response.get('Listing', [])
        results_total = response.get('ResultsTotal', 0)
        page = response.get('Page', 1)
        results_per_page = response.get('ResultsPerPage', 0)
        total_pages = int(math.ceil(results_total/results_per_page))

        print page
        print len(listing), results_total, total_pages
        print listing

        if page < total_pages:
            page += 1
            self.search_products(search_string, page)
