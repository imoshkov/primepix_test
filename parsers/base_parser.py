# -*- coding: utf-8 -*-

import pycurl
from StringIO import StringIO


class ApiKeyDoesntSet(Exception):
    """
    Исключение которое надо вываливать если у парсера не задан ключ API
    """


class RequestMethodDoesntSet(Exception):
    pass


class BaseParser(object):
    """
    Базовый класс парсеров. Описывает общие методы для всех парсеров
    """

    def __init__(self):
        pass

    def _request(self, method, url, headers, post_data=None):
        s = StringIO()
        curl = pycurl.Curl()

        if method == 'get':
            curl.setopt(pycurl.HTTPGET, 1)
        elif method == 'post':
            curl.setopt(pycurl.POST, 1)
            curl.setopt(pycurl.POSTFIELDS, post_data)
        else:
            raise RequestMethodDoesntSet()

        curl.setopt(pycurl.URL, url)

        curl.setopt(pycurl.WRITEFUNCTION, s.write)
        curl.setopt(pycurl.NOSIGNAL, 1)
        curl.setopt(pycurl.CONNECTTIMEOUT, 30)
        curl.setopt(pycurl.TIMEOUT, 80)
        curl.setopt(pycurl.HTTPHEADER, ['%s: %s' % (k, v)
                                        for k, v in headers.iteritems()])

        curl.perform()

        request_body = s.getvalue()
        request_code = curl.getinfo(pycurl.RESPONSE_CODE)
        return request_code, request_body

    def search_products(self, *args, **kwargs):
        raise NotImplementedError()
